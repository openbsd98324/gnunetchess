

/// THIS CODE IS GNU LICENCING
//// IT IS FREE SOFTWARE ::: GNU + Free, Libre Soft


//id-20191230-190150 GNU NET CHESS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <errno.h> 
#include <unistd.h>       //close 
#include <arpa/inet.h>    //close 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
#include <dirent.h>   //struct for ls 
//#include <time.h> 
//#include <unistd.h> 
#if defined(__linux__) //linux
#elif defined(__unix__) 
#define PATH_MAX 2046 
#endif
    
#define TRUE   1 
#define FALSE  0 
#define PORT 8888 
int board[15][15]; 


int revs( int chari ) 
{
   int r = 0; 
   if      ( chari == 1 ) r = 8;
   else if ( chari == 2 ) r = 7;
   else if ( chari == 3 ) r = 6;
   else if ( chari == 4 ) r = 5;
   else if ( chari == 5 ) r = 4;
   else if ( chari == 6 ) r = 3;
   else if ( chari == 7 ) r = 2;
   else if ( chari == 8 ) r = 1;
   return r;
}

int detcol( int chari ) 
{
   int r = 0; 
   if ( ( chari >= 'a' )  && ( chari <= 'z' ) )
      r = 2;
   else if ( ( chari >= 'A' )  && ( chari <= 'Z' ) )
      r = 1;
   return r;
}


/////////////////////////////////
void proc_save_append_env(  char *myout  )
{
        FILE *fptt; 
        char cwd[PATH_MAX];
        char target[PATH_MAX];
        strncpy( target , getcwd( cwd, PATH_MAX ), PATH_MAX );
        strncat( target , "/" , PATH_MAX - strlen( target ) -1 );
        strncat( target , ".gnunetchess.ini" , PATH_MAX - strlen( target ) -1 );
        fptt = fopen( target , "ab+" );
        fputs( myout , fptt );
        fclose( fptt );
}


void clr_board()
{
   int j, i ;  
   for( i = 1 ; i <= 10 ; i++)
   for( j = 1 ; j <= 10 ; j++)
      board[i][j] = '.';

   board[8][1] = 'R';
   board[8][2] = 'N';
   board[8][3] = 'B';
   board[8][4] = 'Q';
   board[8][5] = 'K';
   board[8][6] = 'B';
   board[8][7] = 'N';
   board[8][8] = 'R';

   board[7][1] = 'P';
   board[7][2] = 'P';
   board[7][3] = 'P';
   board[7][4] = 'P';
   board[7][5] = 'P';
   board[7][6] = 'P';
   board[7][7] = 'P';
   board[7][8] = 'P';

   board[1][1] = 'r';
   board[1][2] = 'n';
   board[1][3] = 'b';
   board[1][4] = 'q';
   board[1][5] = 'k';
   board[1][6] = 'b';
   board[1][7] = 'n';
   board[1][8] = 'r';

   board[2][1] = 'p';
   board[2][2] = 'p';
   board[2][3] = 'p';
   board[2][4] = 'p';
   board[2][5] = 'p';
   board[2][6] = 'p';
   board[2][7] = 'p';
   board[2][8] = 'p';
   board[2][8] = 'p';
}



/////////////////////////////////////////////////////////////
int main(int argc , char *argv[])  
{  
   printf( "***************\n" );
   printf( " GNU NET Chess \n" );
   printf( "***************\n" );
   printf( "\n" );
   printf( "Connect with: chessclient 127.0.0.1 8888 \n" );
   printf( "or \n" );
   printf( "Connect with: telnet 127.0.0.1 8888 \n" );

   clr_board();

   int debugm = 0;
   FILE *fptt; 
   char lastmove[1000];
   lastmove[0] = '\0';

   int j, i ;  
   int  chr = 0; 
   char str[PATH_MAX];
   int  fooi=0;
   char string[PATH_MAX];
   char linestr[PATH_MAX];


    int opt = TRUE;  
    int master_socket , addrlen , new_socket , client_socket[30] , max_clients = 30 , activity, valread , sd;  
    int max_sd;  
    struct sockaddr_in address;  

        
    //char buffer[1025];  //data buffer of 1K 
    char buffer[PATH_MAX];  //data buffer of 1K 
        
    //set of socket descriptors 
    fd_set readfds;  
        
    //a message 
    char *message = "ECHO Chess Daemon v1.0 \r\n";  
    
    //initialise all client_socket[] to 0 so not checked 
    for (i = 0; i < max_clients; i++)  
    {  
        client_socket[i] = 0;  
    }  
        
    //create a master socket 
    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0)  
    {  
        perror("socket failed");  
        exit(EXIT_FAILURE);  
    }  
    
    //set master socket to allow multiple connections , 
    //this is just a good habit, it will work without this 
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, 
          sizeof(opt)) < 0 )  
    {  
        perror("setsockopt");  
        exit(EXIT_FAILURE);  
    }  
    
    //type of socket created 
    address.sin_family = AF_INET;  
    address.sin_addr.s_addr = INADDR_ANY;  
    address.sin_port = htons( PORT );  
        
    //bind the socket to localhost port 8888 
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0)  
    {  
        perror("bind failed");  
        exit(EXIT_FAILURE);  
    }  
    printf("Listener on port %d \n", PORT);  
        
    //try to specify maximum of 3 pending connections for the master socket 
    if (listen(master_socket, 3) < 0)  
    {  
        perror("listen");  
        exit(EXIT_FAILURE);  
    }  
        
    //accept the incoming connection 
    addrlen = sizeof(address);  
        

    while(TRUE)  
    {  
        //clear the socket set 
        FD_ZERO( &readfds );  
    
        //add master socket to set 
        FD_SET(master_socket, &readfds);  
        max_sd = master_socket;  
            
        //add child sockets to set 
        for ( i = 0 ; i < max_clients ; i++)  
        {  
            //socket descriptor 
            sd = client_socket[i];  
                
            //if valid socket descriptor then add to read list 
            if(sd > 0)  
                FD_SET( sd , &readfds);  
                
            //highest file descriptor number, need it for the select function 
            if(sd > max_sd)  
                max_sd = sd;  
        }  
    
        //wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely 
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
      
        //if ((activity < 0) && (errno!=EINTR))  
        //{  
            //printf("select error");  
        //}  
            
        //If something happened on the master socket , 
        //then its an incoming connection 
        if (FD_ISSET(master_socket, &readfds))  
        {  
            if ((new_socket = accept(master_socket, 
                    (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)  
            {  
                perror("accept");  
                exit(EXIT_FAILURE);  
            }  
            
            //send new connection greeting message 
            if( send(new_socket, message, strlen(message), 0) != strlen(message) )  
            {  
                perror("send");  
            }  
                
            puts("Welcome message sent successfully");  
                
            //add new socket to array of sockets 
            for (i = 0; i < max_clients; i++)  
            {  
                //if position is empty 
                if( client_socket[i] == 0 )  
                {  
                    client_socket[i] = new_socket;  
                    //printf("Adding to list of sockets as %d\n" , i);  
                        
                    break;  
                }  
            }  
        }  
            
        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++)  
        {  
            sd = client_socket[i];  
            if (FD_ISSET( sd , &readfds))  
            {  
                //Check if it was for closing , and also read the 
                //incoming message 
                if ((valread = read( sd , buffer, 1024)) == 0)  
                {  
                    //Somebody disconnected , get his details and print 
                    getpeername(sd , (struct sockaddr*)&address , \
                        (socklen_t*)&addrlen);  
                    //printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
                        
                    //Close the socket and mark as 0 in list for reuse 
                    close( sd );  
                    client_socket[i] = 0;  
                }  
                    
                //Echo back the message that came in 
                else
                {  
                       //set the string terminating NULL byte on the end 
                       //of the data read 
                       buffer[valread] = '\0';  

                       // cmd /new 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'n') 
                       if ( buffer[2] == 'e') 
                       if ( buffer[3] == 'w') 
                       {
                            clr_board();
                            for( i = 1 ; i <= 8 ; i++)
                            {
                                for( j = 1 ; j <= 8 ; j++)
                                    printf( "%c " , board[i][j] );
                                printf( "\n" ); 
                            }
                       }

                       // cmd /board 
                       if ( buffer[0] == '/') 
                       if ( buffer[1] == 'b') 
                       if ( buffer[2] == 'o') 
                       if ( buffer[3] == 'a') 
                       if ( buffer[4] == 'r') 
                       if ( buffer[5] == 'd') 
                       {
                            for( i = 1 ; i <= 8 ; i++)
                            {
                                for( j = 1 ; j <= 8 ; j++)
                                    printf( "%c " , board[i][j] );
                                printf( "\n" ); 
                            }
                       }


                       strncpy(   string,  ":ECHO:\n"  , PATH_MAX );
                       send( sd , string , strlen( string ) , 0 );  
                       ////
                       strncpy(   string,  buffer , PATH_MAX );
                       send( sd , string , strlen( string ) , 0 );  
                       ////
                       strncpy( string, "\n" , PATH_MAX );
                       send( sd , string , strlen( string ) , 0 );  

                       if ( ( buffer[0] >= 'a') &&  ( buffer[0] <= 'h')) 
                       if ( ( buffer[2] == ' ') || ( buffer[2] == 'x' ) )
                       {
                          if ( debugm == 1 ) printf( "Move\n" );
                          send( sd , "MOVE\n" , strlen( "MOVE\n" ) , 0 );  
                       }


                       // log
                       snprintf( linestr , PATH_MAX , "%d/%d: Input %s", sd, max_clients , buffer );   
                       proc_save_append_env( linestr );


                       // foostr
                       int si = 0; int sj; 
                       int ti = 0; int tj; 
                       char foostr[1000]; 
                       int  prix = ' ';
                       if ( ( buffer[0] >= 'a') &&  ( buffer[0] <= 'h')) 
                       if ( ( buffer[2] == ' ') ||  ( buffer[2] == 'x' ))
                       if ( ( buffer[3] >= 'a') &&  ( buffer[0] <= 'h')) 
                       {
                          si = buffer[0] - 'a' +1 ; 
                          sj = revs( buffer[1] - '1' +1 ) ; 
                          ti = buffer[3] - 'a' +1 ; 
                          tj = revs( buffer[4] - '1' +1 ) ; 

                          if ( board[ sj ][ si ] != '.' ) 
                          {
                              snprintf( foostr, sizeof(foostr), "Player-%d Move: %s", detcol( board[sj][si] ) , buffer );  
                              strncpy( lastmove, foostr, 1000 );
                              send( sd , foostr , strlen( foostr ) , 0 );  

                              printf( "> P%d, move %d %d  %d %d, buffer: %s",  detcol( board[sj][si] ) ,  si, sj , ti , tj, buffer );

                              /// log
                              //snprintf( linestr, PATH_MAX , "> P%d, move %d %d  %d %d, buffer: %s",  detcol( board[sj][si] ) ,  si, sj , ti , tj, buffer );
                              if ( board[ tj ][ ti ] != '.' )  prix = 'x'; else prix='-';
                              snprintf( linestr , sizeof( linestr ) , "%d (Socket %d), Player %d, move %d %d (%c) %c %d %d (%c), [%c%c%c%c%c], buffer: %s",  (int)time(NULL), sd , detcol( board[sj][si] ) ,  si, sj, board[ sj ][ si ] , prix , ti , tj,        board[ tj ][ ti ]  ,  buffer[0] ,  buffer[1] ,  prix  ,  buffer[3]  , buffer[4],  buffer  );
                              proc_save_append_env( linestr );
                              /// move and clean up 
                              board[ tj ][ ti ] = board[ sj ][ si ];
                              board[ sj ][ si ] = '.';
                          }
                       }


                         // new
                         int k ;  int userk = sd ;  char msgout[PATH_MAX];
                         for (k = 0; k < max_clients; k++)                                            
                         {                                                                            
                               // cmd /last 
                               if ( buffer[0] == '/') 
                               if ( buffer[1] == 'l') 
                               if ( buffer[2] == 'a') 
                               if ( buffer[3] == 's') 
                               if ( buffer[4] == 't') 
                               {
                                  send( client_socket[ k ] , "LAST\n" , strlen( "LAST\n" ) , 0 );  
                                  send( client_socket[ k ] , lastmove , strlen( lastmove ) , 0 );  
                               }

                               strncpy( string, "BOARD\n", 1000 );
                               send( client_socket[ k ] , string , strlen( string ) , 0 );                         
                               char charo[1000];
                               char foo[1000]; int fi, fj ; 
                               strncpy(  foo, ""    , 1000 );
                               strncpy(  string, " " , 1000 );
                               for( fi = 1 ; fi <= 8 ; fi++)
                               {
                                    for( fj = 1 ; fj <= 8 ; fj++)
                                    {
                                      strncpy( foo, string , 1000 );
                                      snprintf( string, sizeof(string), "%s%c ", foo, board[ fi ][ fj ] );
                                    }
                                    strncpy( foo, string , 1000 );
                                    snprintf( string, sizeof(string), "%s%c ", foo, '\n' ); 
                               }
                               send( client_socket[ k ] , string , strlen( string ) , 0 );                         






                           if (FD_ISSET( client_socket[ k ]  , &readfds))                                            
                           {  
                           } 
                           else 
                           {
                               snprintf( msgout ,  sizeof( msgout ), "%d Socket %d/%d/%d/%s: %s\n", (int)time(NULL), userk ,  client_socket[ k ] , max_clients,   " " ,  buffer );
                               send( client_socket[ k ] , msgout , strlen( msgout ) , 0 );                         
                               //set the string terminating NULL byte on the end                

                           }
                          }

                }  
            }  
        }  
    }  
        
    return 0;  
}




